import csv
import requests
import os
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException

from dotenv import load_dotenv
from pathlib import Path

from twilio.rest.api.v2010.account import incoming_phone_number


env_path = Path(".") / ".env"
load_dotenv(dotenv_path=env_path)

TWILIO_ACCOUNT_SID = os.environ.get("TWILIO_ACCOUNT_SID", "")
TWILIO_AUTH_TOKEN = os.environ.get("TWILIO_AUTH_TOKEN", "")


def get_all_incoming_phone_numbers():

    account_sid = TWILIO_ACCOUNT_SID
    auth_token = TWILIO_AUTH_TOKEN
    client = Client(account_sid, auth_token)

    with open('output.csv', 'w') as output:
        csv_writer = csv.writer(output)
        csv_writer.writerow(["Phone Number", "SMS Capability", "Current Status"])
        # number of pages=42
        for page_number in range(43):
            print("##> Page: ", page_number)
            url = f"https://api.twilio.com/2010-04-01/Accounts/ACe1f6fe8a5304bc260a3138df995c8b59/IncomingPhoneNumbers.json?Page={page_number}"

            payload={}
            headers = {
            'Authorization': 'Basic QUNlMWY2ZmU4YTUzMDRiYzI2MGEzMTM4ZGY5OTVjOGI1OTpmMzljOWY2ZDExMTM2MGY4MTM4NjZhYmIxNmFlYzFiYQ=='
            }
            get_data = requests.request("GET", url, headers=headers, data=payload)
            json_response = get_data.json()
            # print(response)
            incoming_phone_number = json_response.get("incoming_phone_numbers")

            for i in range(len(incoming_phone_number)):
                sms_capability = incoming_phone_number[i].get("capabilities").get("sms", "ERROR SMS")
                phone_number = str(incoming_phone_number[i].get("phone_number", "ERROR: PHONE NUMBER"))
                status = incoming_phone_number[i].get("status", "ERROR STATUS")
                # print(f"sms: {sms_capability}")
                print(f"phone_number: {phone_number}")
                csv_writer.writerow([phone_number, sms_capability, status])


def getMessage(input_number):

    account_sid = TWILIO_ACCOUNT_SID
    auth_token = TWILIO_AUTH_TOKEN
    client = Client(account_sid, auth_token)

    message_list = client.messages.list(from_=input_number, limit=2)
    # print(message_list)
    if len(message_list) == 0:
        print("NO")
    else:
        print(len(message_list))
        # for item in message_list:
        #     print(item.body)

def getfivenumbers():
    account_sid = TWILIO_ACCOUNT_SID
    auth_token = TWILIO_AUTH_TOKEN
    client = Client(account_sid, auth_token)

    with open('all_numbers.csv', 'w') as output:
        csv_writer = csv.writer(output)
        all_phone_numbers = client.incoming_phone_numbers.list()
        print("\n=====================")
        for number in all_phone_numbers:
            # print(number.phone_number)
        # for numbers in all_phone_numbers:
            csv_writer.writerow([number.phone_number])

def main():
    # input your file name here
    getfivenumbers()
    # getMessage(6156175943)

    # messages = client.messages.list(limit=20)

    # for record in messages:
    #     print(record.sid)
    # messages = client.messages('SM95bc078389b947879f8a6b3c96854a96').fetch()
    # print(messages)
    # print(messages.from_)
    # print(messages.to)
    # print(messages.status)
    # print(messages.price)


if __name__ == "__main__":
    main()

        