import csv
from typing import Optional

def shift():
    with open("numbers_with_owner_names.csv", "r") as input, \
        open("numbers_shifted.csv", "w") as output:

        input_reader = csv.reader(input, delimiter=",")
        csv_writer = csv.writer(output)

        for line in input_reader:
            if not line[1].startswith("own"):
                temp = line[1]
                line[1] = line[2]
                line[2] = temp
            
            csv_writer.writerow(line)
    
if __name__=="__main__":
    shift()