import requests
import time
import json
import csv

def getinfo():
    with open("owner_no_name.csv", "r") as input, \
        open("ownerid_and_names.csv", "w") as output:

        inputreader = csv.reader(input, delimiter=",")
        csv_writer = csv.writer(output)

        for line in inputreader:
            id = line[0]
            url = f"http://tenantapi.com/v3/owners/{id}"
            # print(url)

            payload={}
            headers = {
            'X-storageapi-key': 'ff9bfdaa3f6e439382ad24274a991c57',
            'X-storageapi-date': str(int(time.time()))
            }

            response = requests.request("GET", url, headers=headers, data=payload)
            json_response = response.json()

            get_name=  json_response["data"]["owner"].get("name", "ERROR")
            print(id, get_name)
            csv_writer.writerow([id,get_name])

        # print(response.text)


if __name__=="__main__":
    getinfo()