import csv

def comapre():
    with open("twilio_numbers.csv", "r") as twilio_input, \
        open("dbinput.csv", "r") as db_input, \
        open("common_numbers.csv", "w") as output:

        twilioreader = csv.reader(twilio_input, delimiter=',')
        dbreader = csv.reader(db_input, delimiter=',')
        csv_writer = csv.writer(output)

        number_list= []
        processed_dblist = 0
        processed_twilio = 0

        number_common = 0

        for line in twilioreader:
            processed_twilio +=1
            number = line[0]
            number_list.append(number)

        print(f"##> twilio_list: {processed_twilio}")
        
        for line in dbreader:
            processed_dblist +=1
            number = line[0]
            if number in number_list:
                number_common +=1
                csv_writer.writerow(line)
            
        print(f"##> db_list: {processed_dblist}")

        print(f"\nSummary::")
        print(f"Number_db_list: {processed_dblist}")
        print(f"Number_twilio_list: {processed_twilio}")
        print(f"number_common: {number_common}")



if __name__=="__main__":
    comapre()