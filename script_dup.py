import csv
import json


def find_duplicates():
    with open("script_input.csv", "r") as input, \
        open("script_duplicate.csv", "w") as duplicate, \
        open("script_output.csv", "w") as output:

        inputreader = csv.reader(input, delimiter=",")
        duplicatewriter = csv.writer(duplicate)
        outputwriter= csv.writer(output)

        list_all = []
        list_duplicate = []

        number_of_unique = 0
        number_of_duplicate = 0
        number_of_all = 0

        for line in inputreader:
            # print(line)
            number = line[0]
            number_of_all+=1
            if number in list_all:
                number_of_duplicate +=1
                list_duplicate.append(number)
                # duplicatewriter.writerow(line)
                print(f"##> Duplicate: {line}")
            else:
                list_all.append(number)
                number_of_unique +=1
                outputwriter.writerow(line)
        
        for number in list_duplicate:
            input.seek(0)
            for line in inputreader:
                if line[0] == number:
                    duplicatewriter.writerow(line)

        print(f"\nSummary::")
        print(f"Number_all: {number_of_all}")
        print(f"Number_duplicate: {number_of_duplicate}")
        print(f"Number_unique: {number_of_unique}")

        # print(json.dumps(dict_duplicate, indent=4))



if __name__ == "__main__":
    find_duplicates()