import csv


def find_duplicates():
    with open("v1_input.csv", "r") as v1input,\
        open("v2_input.csv", "r") as v2input, \
        open("output_all_numbers.csv", "w") as output:
    # with open("twilio_numbers.csv", "r") as twilioinput,\
    #     open("unique_v1_v2.csv", "r") as v1v2input, \
    #     open("duplicate_from_twilio_and_v1_v2.csv", "w") as duplicate, \
    #     open("unique_twilio_db.csv", "w") as output:
        #  open("unique_people_list.csv", "w") as unique_people_csv:

        v1inputreader = csv.reader(v1input, delimiter=",")
        v2inputreader = csv.reader(v2input, delimiter=",")
        # csv_all_writer = csv.writer(all_people_csv)
        # duplicate = csv.writer(duplicate)
        csv_writer = csv.writer(output)

        v1inputlist = []
        v2inputlist = []
        uniqueList = []

        number_of_unique = 585
        number_of_all = 585

        #1113
        for line in v2inputreader:
            uniqueList.append(line[0])
            # print(line[0])
            csv_writer.writerow(line)
        # print(f"v1inputlist: {len(v1inputlist)}")

        #585
        d=0
        for line in v1inputreader:
            number = str(line[0])
            if number in uniqueList:
                d = d+1
                print(f"##> Duplicate: {line}")
            else:
                number_of_unique +=1
                uniqueList.append(number)
                csv_writer.writerow(line)
            number_of_all +=1
            # v2inputlist.append(line[0])
            
        # print(f"v2inputlist: {len(v2inputlist)}")

        # print(f"duplicate: {d}")
        # uniqueList = v1inputlist
        # print(f"uniqueList: {len(uniqueList)}")

        # n =2117
        # d =0
        # v2input.seek(0)
        # for number in v2inputlist:
        #     if number in 
        #     # print(number)
        #     n += 1
        #     if number in uniqueList:
        #         print(f"##> Duplicate: {number}")
        #         # duplicate.writerow([number])
        #         d+=1
        #     else:
        #         csv_writer.writerow([number])

        # print(f"duplicate: {d}")
        # print(f"total: {n}")


        #duplicate
        # for line in v1reader:
        #     number = str(line[0])
        #     if number.startswith("+1"):
        #         number = number[2:]

        #     # print(number)
        #     if number in v2list:
        #         print(f"##> Duplicate: {line}")
        #     else:
        #         v2list.append(number)
        #         csv_writer.writerow([number])

        print(f"\nSummary::")
        print(f"Number_all: {number_of_all}")
        print(f"Number_duplicate: {d}")
        print(f"Number_unique: {number_of_unique}")


if __name__ == "__main__":
    find_duplicates()

'''
6156496734
6156175456
6156564313
6158234918
6159885045
6153488691
6159539922
6152837248
2709368506
6156063205
6159004818
6153691984
6156496125
6159884811
6156244761
6156249635
6156175943
6159884001
3606376163
5095900373
5094166200
2535011703
5095900327
5092287598
5094245347
'''