import csv


def find_duplicates():
    # with open("v1_input.csv", "r") as v1input,\
    #     open("v2_output.csv", "r") as v2input, \
    #     open("output_all_numbers.csv", "w") as output:
    with open("unique_twilio_db.csv", "r") as uinputfile,\
        open("v1_input.csv", "r") as v1inputfile, \
        open("v2_input.csv", "w") as v2inputfile, \
         open("FINAL.csv", "w") as final:

        uinput = csv.reader(uinputfile, delimiter=",")
        v1input = csv.reader(v1inputfile, delimiter=",")
        v2input = csv.reader(v2inputfile, delimiter=",")
        # csv_all_writer = csv.writer(all_people_csv)
        csv_writer = csv.writer(final)

        uinputlist = []
        v1v2inputlist = []
        uniqueList = []

        number_of_unique = 0
        number_of_all = 0

        #2126
        for line in uinput:
            number = str(line[0])
            # if number.startswith("+1"):
            #     number = number[2:]
            uinputlist.append(number)
            # print(line[0])
            # csv_writer.writerow([number])
        print(f"uinputlist: {len(uinputlist)}")

        #1673
        # for line in v1v2input:
        #     v1v2inputlist.append(line[0])
        #     # print(line[0])
        # print(f"v1v2inputlist: {len(v1v2inputlist)}")

        # uniqueList = twilioinputlist
        # print(f"uniqueList: {len(uniqueList)}")

        # n =2117
        # d =0
        for number in uinputlist:
            v2inputfile.seek(0)
            v1inputfile.seek(0)
            if number in uniqueList:
                print(f"##> Duplicate: {number}")
                d+=1
            else:
                csv_writer.writerow([number])

        # print(f"duplicate: {d}")
        # print(f"total: {n}")


        #duplicate
        # for line in v1reader:
        #     number = str(line[0])
        #     if number.startswith("+1"):
        #         number = number[2:]

        #     # print(number)
        #     if number in v2list:
        #         print(f"##> Duplicate: {line}")
        #     else:
        #         v2list.append(number)
        #         csv_writer.writerow([number])



if __name__ == "__main__":
    find_duplicates()

'''
6156496734
6156175456
6156564313
6158234918
6159885045
6153488691
6159539922
6152837248
2709368506
6156063205
6159004818
6153691984
6156496125
6159884811
6156244761
6156249635
6156175943
6159884001
3606376163
5095900373
5094166200
2535011703
5095900327
5092287598
5094245347
'''