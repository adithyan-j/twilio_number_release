"""Releases number from twilio account

Reads each line of the input file, gets the phone number
and calls the deletes function from twilio python module

Returns an output file with Status of each number

params:
    -name_of_input_file: str


REQUIREMENTS:
    twilio==6.63.0
    python-dotenv==0.14.0

    install with pip  
"""

import csv
import os
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException

from dotenv import load_dotenv
from pathlib import Path

env_path = Path(".") / ".env"
load_dotenv(dotenv_path=env_path)

TWILIO_ACCOUNT_SID = os.environ.get("TWILIO_ACCOUNT_SID", "")
TWILIO_AUTH_TOKEN = os.environ.get("TWILIO_AUTH_TOKEN", "")


def delete_number(name_of_input_file):

    with open(name_of_input_file, "r") as input, open("status_of_released_numbers.csv", "w") as ouput:

        csv_reader = csv.reader(input, delimiter=",")
        csv_writer = csv.writer(ouput)

        # get the line of column names and write the appended names to output file
        column_names = next(csv_reader)
        column_names.append("Status")

        csv_writer.writerow(column_names)

        input_line_number = 0
        number_of_success = 0
        number_of_release_failed = 0
        number_of_no_details = 0
        number_of_errors = 0

        account_sid = TWILIO_ACCOUNT_SID
        auth_token = TWILIO_AUTH_TOKEN

        # loop through each line in csv file
        for line in csv_reader:

            input_line_number = input_line_number + 1
            message = ""
            # get the phone number
            twilio_phone_number = line[0]
            print(f"##> Input: {input_line_number} Phone number: {twilio_phone_number}")

            # if the phone number starts with + then do nothing, else add + at the beginning
            starts_with_plus = twilio_phone_number.startswith("+")
            if not starts_with_plus:
                twilio_phone_number = "+" + twilio_phone_number

            try:
                client = Client(account_sid, auth_token)
                twilio_phone = client.incoming_phone_numbers.list(
                    phone_number=twilio_phone_number, limit=1
                )
                try:
                    twilio_phone_sid = twilio_phone[0].sid
                    twilio_delete = client.incoming_phone_numbers(
                        twilio_phone_sid
                    ).delete()
                    if twilio_delete:
                        print(f"SUCCESS: Twilio number({twilio_phone_number}) released")
                        message += "SUCCESS"
                        number_of_success += 1
                        # return twilio_delete
                    else:
                        print(
                            f"ERROR: Twilio number({twilio_phone_number}) released failed"
                        )
                        message += "Release failed"
                        number_of_release_failed += 1
                except IndexError:
                    print(
                        f"ERROR: Twilio number({twilio_phone_number}) details not found"
                    )
                    message += "Details not found"
                    number_of_no_details += 1
            except TwilioRestException as e:
                print(f">> ERROR: Unexpected Exception occured. Msg:{e}")
                number_of_errors += 1

            print("\n")
            line.append(message)
            csv_writer.writerow(line)

        # print the final result
        print(
            f"\n###>    SUMMARY \n\
            Total: {input_line_number} \n\
            Success: {number_of_success}\n\
            Release failed: {number_of_release_failed}\n\
            Details not found: {number_of_no_details}\n\
            Errors: {number_of_errors}\n"
        )


def main():
    # input your file name here
    delete_number("numbers_to_be_released.csv")


if __name__ == "__main__":
    main()
